<?php

class MapModel {
    
    private $db;

    public function __construct($db) {
        $this->db = $db;
    }

    public function getByBboxLL($bbox) {
        $sql ="SELECT osm_id,highway,name,foot,horse,bicycle,access,ST_AsText(ST_Transform(way, 4326)) AS tway FROM planet_osm_line WHERE way && ST_Transform(ST_SetSRID('BOX3D(".$bbox[0]." ".$bbox[1].",". $bbox[2]." ". $bbox[3].")'::box3d,4326), 3857) AND highway<>''";
        return $this->execBboxSql($sql);
    }

    private function execBboxSql($sql) {
        $dbres = $this->db->query($sql);
        $rows = $dbres->fetchAll(PDO::FETCH_ASSOC);
        return [ "type" => 'FeatureCollection',
                "features" => array_map ( function($row)  {
                        $feature = [
                            "type"=> "Feature", 
                            "properties"=>[],
                            "geometry"=>[
                                "type"=> "LineString", 
                                "coordinates"=>[]
                            ]
                        ];
                        $matches = [];
                        preg_match("/LINESTRING\(([-\d\., ]+)\)/",$row["tway"], $matches);
                        foreach($row as $k=>$v) {
                            if($k !="tway") {
                                $feature["properties"][$k] = $row[$k];
                            }
                        }    

                        $lls = explode(",", $matches[1]);
                        foreach($lls as $ll) {
                            $feature["geometry"]["coordinates"][] = array_map( 
                                function($value) { return (float)$value; }, 
                                explode(" ", $ll) 
                            );
                        }
                        return $feature;
                }, $rows)
            ];
    }

    public function findNearestHighway($lon, $lat, $distThreshold) {
        $stmt = $this->db->prepare("SELECT ST_AsText(ST_Transform(ST_ClosestPoint(way, transformed), 4326)) AS closest, highway, waydist FROM (SELECT *, ST_Distance(way, transformed) AS waydist FROM planet_osm_line, (SELECT ST_Transform(ST_GeomFromText('POINT($lon $lat)', 4326), 3857) AS transformed) AS tquery WHERE way && ST_Transform(ST_SetSRID('BOX3D(".($lon-0.01)." ".($lat-0.01).",".($lon+0.01)." ".($lat+0.01).")'::box3d, 4326), 3857) ) AS dquery WHERE highway<>'' AND waydist < ? ORDER BY waydist LIMIT 1");
        $stmt->execute([$distThreshold]);
        $highway = $stmt->fetch(PDO::FETCH_ASSOC);
        if($highway !== false) {
            $m = [];
            preg_match("/POINT\((-?[\d\.]+) (-?[\d\.]+)\)/", $highway["closest"], $m);
            $highway["highwayLon"] = $m[1];
            $highway["highwayLat"] = $m[2];
            unset($highway["closest"]);
        } else {
            $highway = [];
        }
        return $highway;
    }
}
?>
