CREATE TABLE panoramas(id serial, the_geom geometry, poseheadingdegrees FLOAT, authorised INT DEFAULT 0, userid INT DEFAULT 0);
CREATE TABLE users (id serial, username VARCHAR(255), password VARCHAR(255), isadmin INT DEFAULT 0);

