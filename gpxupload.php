<?php

session_start();

header("Content-type: application/json");

require_once('defines.php');
require_once(LIBDIR.'/functionsnew.php');
require_once(LIBDIR.'/gpx.php');

$positioned = ["panos"=>[]];
if(!isset($_SESSION['userid'])) {
    header("HTTP/1.1 401 Unauthorized");
    $positioned["error"] = "Not logged in.";
}

$conn = new PDO("pgsql:host=localhost;dbname=".DB_DBASE, DB_USER);
if(isset($_FILES["file"]) && !isset($positioned["error"])) {
    $u = upload_file("file",OTV_GPX_UPLOADS, "gpx_{$_SESSION["userid"]}_".time().".gpx");
    if($u["file"]!==null) {
        $gpx = file($u["file"]);
        unlink($u["file"]);
        if($gpx!==false) {
            $gpx = parseGPX($gpx);

            if($gpx!==false) {
                $mintime = PHP_INT_MAX;
                $maxtime = 0;
                $avglat = 0;
                $avglon = 0;
    
                foreach($gpx['trk'] as $trkpt) {
                    $avglat += $trkpt["lat"];
                    $avglon += $trkpt["lon"];

                    if($trkpt['time'] < $mintime) {
                        $mintime = $trkpt['time'];
                    }
                    if($trkpt['time'] > $maxtime) {
                        $maxtime = $trkpt['time'];
                    }
                }
                $avglat /= count($gpx['trk']);
                $avglon /= count($gpx['trk']);
                $utcOffset = 0; 

                // get the timezone from geonames for the average lat/lon
                $result = json_decode(file_get_contents("http://api.geonames.org/timezoneJSON?lat=$avglat&lng=$avglon&username=otv360"));
                if($result->status) { 
                    $positioned["error"] = $result->status["message"];
                } else { 
                    $serverTimezone = date_default_timezone_get();
                    date_default_timezone_set($result->timezoneId);
                    $utcOffset = ($result->rawOffset+(int)date('I', $mintime))*3600;
                    // restore timezone - Must be set in php.ini (date.timezone)
                    date_default_timezone_set($serverTimezone);
                   
                    $stmt = $conn->prepare("SELECT * FROM panoramas where userid=? AND the_geom IS NULL AND timestamp BETWEEN ? AND ?");
                    $stmt->execute([$_SESSION["userid"], $mintime+$utcOffset, $maxtime+$utcOffset]);
                    $pan = $stmt->fetchAll(PDO::FETCH_ASSOC);
                    //print_r($pan);
                    for($i=0; count($positioned["panos"])<count($pan) && $i<count($gpx["trk"])-1; $i++) {
                        $dTime = $gpx["trk"][$i+1]["time"] - $gpx["trk"][$i]["time"];
                        foreach($pan as $curpan) {
                            $panoTimestampUtc = $curpan["timestamp"] - $utcOffset;
                            if($panoTimestampUtc>=$gpx["trk"][$i]["time"] && $panoTimestampUtc<=$gpx["trk"][$i+1]["time"]) {
                                $prop=($panoTimestampUtc - $gpx["trk"][$i]["time"])/$dTime;
                                $lat = $gpx["trk"][$i]["lat"] + $prop* ($gpx["trk"][$i+1]["lat"]-$gpx["trk"][$i]["lat"]);
                                $lon = $gpx["trk"][$i]["lon"] + $prop* ($gpx["trk"][$i+1]["lon"]-$gpx["trk"][$i]["lon"]);

                                $positioned["panos"][$curpan["id"]]= ["lat"=>$lat, "lon"=>$lon];
                            }
                        }
                    }
                }
            } else {
                $positioned["error"] = "Could not parse GPX.";
            }
        } else {
            $positioned["error"] = "Could not read GPX file.";
        }
    } else {
        $positioned["error"] = $u["error"];
    }
} else {
    $positioned["error"] = "No GPX file.";
}
echo json_encode($positioned);
?>
