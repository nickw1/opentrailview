const Dialog = require('jsfreemaplib').Dialog;
const Uploader = require('./uploader');

class PhotoManager {

    constructor(nrows, ncols, photodiv, options) {
        this.selectedPhoto = 0;
        this.positionedPhotos = {};
        this.nrows = nrows;
        this.ncols = ncols;
        this.currentPage = 0;
        this.curPos = null;
        this.options = options || {};
        this.options.adminProvider = this.options.adminProvider || { isadmin: 0 };
        this.table = document.createElement("table");

        this.endpoints = {
            'mine': 'panos/mine',
            'unauthorised' : 'panos/unauthorised',
            'unpositioned' : 'panos/unpositioned'
        };

        var upload = document.createElement('input');
        upload.type = 'button';
        upload.value = 'Upload positioned panos';
        upload.id='uploadPositionedPhotos';
        upload.disabled = true; 
        upload.addEventListener('click', this.uploadPositionedPhotos.bind(this)); 
        options.actionsContainer.appendChild(upload);

        this.controlsDiv = document.createElement('div');
        var rewindBtn = document.createElement('a');
        rewindBtn.href='#';
        rewindBtn.id = 'rewindBtn';
        rewindBtn.appendChild(document.createTextNode('<<'));
        rewindBtn.addEventListener('click', this.displayCurrentPage.bind(this, 0));
        rewindBtn.style.display = 'none';
        this.controlsDiv.appendChild(rewindBtn);
        this.controlsDiv.appendChild(document.createTextNode(" "));

        var backBtn = document.createElement('a');
        backBtn.href='#';
        backBtn.id = 'backBtn';
        backBtn.appendChild(document.createTextNode('<'));
        backBtn.addEventListener('click', this.displayPreviousPage.bind(this));
        backBtn.style.display = 'none';
        this.controlsDiv.appendChild(backBtn);
        this.controlsDiv.appendChild(document.createTextNode(" "));

        this.pageNavigation = document.createElement('span');
        this.controlsDiv.appendChild(this.pageNavigation);

        var nextBtn = document.createElement('a');
        nextBtn.href='#';
        nextBtn.id = 'nextBtn';
        nextBtn.appendChild(document.createTextNode('>'));
        nextBtn.addEventListener('click', this.displayNextPage.bind(this));
        nextBtn.style.display = 'none';
        this.controlsDiv.appendChild(nextBtn);
        this.controlsDiv.appendChild(document.createTextNode(" "));

        var fastForwardBtn = document.createElement('a');
        fastForwardBtn.href='#';
        fastForwardBtn.id = 'fastForwardBtn';
        fastForwardBtn.appendChild(document.createTextNode('>>'));
        fastForwardBtn.addEventListener('click', this.displayLastPage.bind(this));
        fastForwardBtn.style.display = 'none';
        this.controlsDiv.appendChild(fastForwardBtn);


        var status = document.createElement("div");
        status.classList.add("error");
        status.id = "photoMgrStatus";
        document.getElementById(photodiv).appendChild(status);

        this.select = document.createElement("select"); 
        this.select.id = 'panoSelectionModes';


        document.getElementById(photodiv).appendChild(this.select);
        document.getElementById(photodiv).appendChild(this.table);
        document.getElementById(photodiv).appendChild(this.controlsDiv);
        this.reloadModeSelect();
        this.loadPanosByCurrentCriterion();
        this.select.addEventListener("change", e=> {
            this.loadPanosByCriterion(e.target.value)
        });
    }

    reloadModeSelect() {
        this.select.innerHTML = "";

        var modeVals = { 
                            "mine": "My panoramas",
                           "unpositioned": "My unpositioned panoramas" 
                        };
        for(k in modeVals) {
            var option = document.createElement("option");
            option.value = k;
            option.appendChild(document.createTextNode(modeVals[k]));
            this.select.appendChild(option);
        }


        if(this.options.adminProvider.isadmin == 1) {
            var option = document.createElement("option");
            option.value = 'unauthorised';
            option.appendChild(document.createTextNode('Unauthorised panoramas'));
            this.select.appendChild(option);

        }
    }    

    setCoords(latlng) {
        if(this.selectedPhoto> 0) {
            this.curPos = latlng;
            this.latLonUpdated();
        } 
    }

    latLonUpdated() { 
        document.getElementById(`img${this.selectedPhoto}`).style.border='5px solid red';
        this.positionPhoto(this.selectedPhoto, this.curPos.lat, this.curPos.lng);
        this.curPos = null;
    }
    
    positionPhoto(id, lat, lon) {
        document.getElementById('uploadPositionedPhotos').disabled = false;
        this.positionedPhotos[id] = {'lat': lat, 'lon': lon};
        document.getElementById('lat'+id).innerHTML = 'Lat: '+lat.toFixed(4) + ', ';
        document.getElementById('lon'+id).innerHTML = 'Lon: '+lon.toFixed(4) + ' ';
        if(this.options.onPositioned) {
            this.options.onPositioned(id, lat, lon);
        }
    }

    uploadPositionedPhotos() {
        if(Object.keys(this.positionedPhotos).length > 0) {    
            fetch(`panoramas/move`,

                                { body: JSON.stringify(this.positionedPhotos),
                                headers: { 'Content-Type': 'application/json'},
                                method:'POST'}).
                                then(response=>response.json()).
                                then(successful=>{ 
                                    this.positionedPhotos = {};
                                    this.status(`Successfully uploaded ${successful.length} positions.`);
                                    document.getElementById('uploadPositionedPhotos').disabled = true;
                                    this.loadPanosByCriterion(document.getElementById('panoSelectionModes').value);
                                    if(this.options.onPositionUploaded) {
                                        this.options.onPositionUploaded();
                                    }
                                });
            
        }    
    }

    photoClick(e) {
        this.removeSelected();
        e.target.style.borderColor='red';    
        var id = e.target.id.substr(3);
        this.selectedPhoto = e.target.id.substr(3);
        if(this.options.onSelected) {
            this.options.onSelected(id);
        }
    } 

    removeSelected() {
        if(this.selectedPhoto > 0) {
            var el=document.getElementById('img'+this.selectedPhoto);
            if(el) {
                el.style.borderColor='black';
                this.selectedPhoto = 0;
            }
        }
    }

    loadPanosByCurrentCriterion() {
        this.loadPanosByCriterion(this.select.value);
    }

    loadPanosByCriterion(criterion) {
        fetch(this.endpoints[criterion]).then(resp=> {
                if(resp.status==200) {
                    return resp.json();
                } else if (resp.status==401) {
                    throw new Error('Your session has timed out. Please log in again.');
                }
            }).then(data => {
            this.userPhotos = data;
            this.nPages = 1+Math.floor((this.userPhotos.length-1) / (this.nrows * this.ncols));
            this.pageNavigation.innerHTML = '';
            this.displayCurrentPage(0);
            document.getElementById('rewindBtn').style.display = this.nPages > 0 ? 'inline': 'none';
            document.getElementById('fastForwardBtn').style.display = this.nPages > 0 ? 'inline': 'none';
        }); //.catch(e => this.status(e));
    }

    displayCurrentPage(pg) {
        this.currentPage=pg;
        this.doDisplayCurrentPage();
    }

    doDisplayCurrentPage() {
        if(this.nPages > 0) {
            this.setupPageLinks();
        }
        this.table.innerHTML =  "";
        var startIdx = this.currentPage * this.nrows * this.ncols;
        for(var trow=0; trow<this.nrows; trow++) {
            var tr = document.createElement("tr");
            for(var tcol=0; tcol<this.ncols; tcol++) {
                var curIdx = startIdx + (trow*this.ncols+tcol);
                if(curIdx < this.userPhotos.length) {
                    var id = this.userPhotos[curIdx].id;
                    if(this.userPhotos[curIdx].lat && this.userPhotos[curIdx].lon) {
                        // TODO make sure that shows lat/lon
                    }

                    var isSelected=false;
                    var angle=null;
                    if(this.selectedPhoto==id) {
                        isSelected=true;
                    }

                    var td=document.createElement("td");
                    td.id = `td${id}`;
                    var div = document.createElement("div");
                    var img = document.createElement("img");
                    img.setAttribute("class", "photo");
                    img.id = `img${id}`;
                    img.style.border = '5px solid ' +(isSelected?'red':'black');
                    img.src = `panorama/${id}.r10.jpg`;
                    img.alt = `pano ${id}`;
                    div.appendChild(img);
                    td.appendChild(div);
                    var idDisplay = document.createElement("strong");
                    idDisplay.innerHTML = `#${id}: `;
                    td.appendChild(idDisplay);
                    var spanLat = document.createElement("span");
                    spanLat.id=`lat${id}`;
                    spanLat.appendChild(document.createTextNode(`Lat ${this.positionedPhotos[id] ? this.positionedPhotos[id].lat.toFixed(4) : (this.userPhotos[curIdx].lat  ? parseFloat(this.userPhotos[curIdx].lat).toFixed(4) : "?")}`));
                    td.appendChild(spanLat);
                    td.appendChild(document.createTextNode(", "));
                    var spanLon = document.createElement("span");
                    spanLon.id=`lon${id}`;
                    spanLon.appendChild(document.createTextNode(`Lon ${this.positionedPhotos[id] ? this.positionedPhotos[id].lon.toFixed(4) : (this.userPhotos[curIdx].lon ? parseFloat(this.userPhotos[curIdx].lon).toFixed(4) :  "?")}`));
                    td.appendChild(spanLon);
                    td.appendChild(document.createElement("br"));
                    var aView = document.createElement('a');
                    aView.href=`?id=${id}`;
                    aView.appendChild(document.createTextNode("View"));
                    td.appendChild(aView);
                    if(this.options.adminProvider.isadmin == 1 && this.userPhotos[curIdx].authorised === 0) {
                        var aAuth = document.createElement('a');
                        aAuth.href='#';
                        aAuth.appendChild(document.createTextNode('Authorise'));
                        aAuth.addEventListener('click', (function(curIdx) {
                            this.status('Authorising...');
                            var id = this.userPhotos[curIdx].id;
                            fetch(`panorama/${id}/authorise`, { method: 'POST' }).then( response => { 
                                if(response.status==200) {
                                    this.status(`Pano ${id} authorised successfully.`);
                                    this.userPhotos.splice(curIdx, 1);
                                    this.doDisplayCurrentPage();
                                } else {
                                    this.status(`Server error: code ${response.status}`);
                                }
                            });
                        }).bind(this, curIdx));
                        td.appendChild(document.createTextNode(" | "));
                        td.appendChild(aAuth);
                    }
                    var aDelete = document.createElement('a');
                    aDelete.href='#';
                    aDelete.appendChild(document.createTextNode('Delete'));
                    aDelete.addEventListener('click', (function(curIdx) {
                        this.status('Deleting...');
                        var id = this.userPhotos[curIdx].id;
                        fetch(`panorama/${id}`, { method: 'DELETE' }).then( response=> {
                            if(response.status==200) {
                                this.status(`Pano ${id} deleted.`);
                                this.userPhotos.splice(curIdx, 1);
                                this.doDisplayCurrentPage();
                            } else {
                                this.status(`Server error: code ${response.status}`);
                            }
                        });
                    }).bind(this, curIdx));
                    td.appendChild(document.createTextNode(" | "));
                    td.appendChild(aDelete);

                    tr.appendChild(td);
                  } 
            }
            this.table.appendChild(tr);
        }


        document.getElementById('backBtn').style.display = (this.currentPage>0 ? 'inline': 'none');
        
        document.getElementById('nextBtn').style.display = (this.currentPage < this.nPages-1) ? 'inline': 'none';  
        this.setupEvents();
    }

    displayPreviousPage() {
        this.displayCurrentPage(this.currentPage-1);
    }

    displayNextPage() {
        this.displayCurrentPage(this.currentPage+1);
    }

    displayLastPage() {
        this.displayCurrentPage(this.nPages - 1);
    }

    setupEvents() {
        var allPhotos = document.querySelectorAll('.photo');
        for(var i=0; i<allPhotos.length; i++) {
            allPhotos[i].addEventListener("click", this.photoClick.bind(this));
        }
    }

    setupPageLinks() {
        this.pageNavigation.innerHTML = '';
        const inclNPages = 10;
        let pageArray = new Array(inclNPages).fill().map( (val,idx)=> idx + Math.min(this.currentPage,this.nPages - inclNPages)).filter( val => val >= 0 && val < this.nPages);
        for(let i of pageArray) {
            const a = document.createElement('a');
            if(i == this.currentPage) {
                a.style.fontWeight = 'bold';
            }
            a.href='#';
            a.appendChild(document.createTextNode(i+1));
            a.addEventListener("click", this.displayCurrentPage.bind(this, i));
            this.pageNavigation.appendChild(a);
            this.pageNavigation.appendChild(document.createTextNode(" "));
        }
    }

    status(msg) {
        document.getElementById('photoMgrStatus').innerHTML = msg;
    } 
}

module.exports = PhotoManager;
