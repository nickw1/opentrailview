const openpanos = require('openpanos-client');
const MapManager = require('./map');
const Uploader = require('./uploader');
const PhotoManager = require('./photomgr');
const Dialog = require('jsfreemaplib').Dialog;
const Filterer = require('./filterer');

class OTV360 {
    constructor(zoom, resize) {
        this.panoView = new openpanos.Client({
            container: '#panorama',
            resizePano: resize,
            api: {
                nearest: 'nearest/{lon}/{lat}',
                byId: 'panorama/{id}',
                panoImg: 'panorama/{id}.jpg',
                panoImgResized:  'panorama/{id}.r{resize}.jpg',
                geojson: 'map',
                nearby: 'panorama/{id}/nearby'
            }
           }); 
        this.setupMediaQueries();
        this.setupAbout();

        this.setupUpload();
        this.setupMap(zoom);
        this.setupModes();
        this.panoView.on('locationChanged',(lon,lat)=> {
            this.lon = lon;
            this.lat = lat;
            if(this.mapMgr) {
                this.mapMgr.setView([lat,lon]/*, zoom*/);
            }
        });
        this.panoView.on("panoChanged", this.checkAuthorised.bind(this));
        this.setupSignup();
        this.setupLogin();
        this.setupSearch();
        this.setupReportPrivacy();
        this.lat = -181;
        this.lon = -91;
    }


    async checkAuthorised(id) {
        if(this.isadmin) {
            const json = await fetch(`panorama/${id}`)
                .then(response => response.json());
            document.getElementById('authorisePano').style.display = (json.authorised === 1 ? 'none': 'inline');
        }
    }

    setupModes () {
        document.getElementById("switchModeImg").addEventListener("click", this.switchMode.bind(this));
        this.setupMode(0);
    }

    switchMode() {
        this.setupMode(this.mode==0 ? 1:0);
    }

    setupMode(newMode, loadCentrePano=true) {
        var images = ['images/baseline_panorama_white_18dp.png', 'images/baseline_map_white_18dp.png'], alts = ['Panorama', 'Map'];
        document.getElementById('switchModeImg').src = images[newMode==0 ? 1:0];
        document.getElementById('switchModeImg').alt = alts[newMode==0 ? 1:0];
        document.getElementById('switchModeImg').title = alts[newMode==0 ? 1:0];
        

        switch(newMode) {
            case 0:
                document.getElementById('panorama').style.display = 'block';
                document.getElementById('drag').style.display = 'none';
                document.getElementById('rotate').style.display = 'none';
                document.getElementById('delete').style.display = 'none';
                document.getElementById('select').style.display = 'none';
                document.getElementById('searchContainer').style.display = 'none';
                this.setupMapPreview();
                if(this.userid) {
                    document.getElementById('uploadpano').style.display = 'inline';
                }
                
                if(this.mode==1 && loadCentrePano === true) {
                    var mapCentre = this.mapMgr.map.getCenter();
                    fetch(`/nearest/${mapCentre.lng}/${mapCentre.lat}`).then(response => response.json()).then (data=> {
                        if(data.id != this.panoView.curPanoId) {
                            this.panoView.moveTo(data.id);
                        }
                    });
                }
                
                break;

            case 1:
                
                document.getElementById('panorama').style.display = 'none';
                document.getElementById('uploadpano').style.display = 'none';
                document.getElementById('map').classList.remove('preview');
                this.mapMgr.map.invalidateSize();
                document.getElementById('select').style.display = 'inline';
                document.getElementById('searchContainer').style.display = 'block';
                if(this.userid) {
                    document.getElementById('drag').style.display = 'inline';
                    document.getElementById('rotate').style.display = 'inline';
                    document.getElementById('delete').style.display = 'inline';
                }
                this.mapMgr.setView([this.lat, this.lon]);
                break;

        }
        this.mode = newMode;
    }

    setupMap(zoom) {

        if(!this.mapMgr) {
            this.mapMgr = new MapManager({userProvider: this,
                                onPanoMarkerClick:id=> { 
                                    this.setupMode(0, false);
                                    this.panoView.moveTo(id);
                                },
                                onPanoChange: this.panoView.update.bind(this.panoView),
                                onMapChange: (centre,zoom)=> {
                                        localStorage.setItem('lat', centre.lat);
                                        localStorage.setItem('lon', centre.lng);
                                        localStorage.setItem('zoom', zoom);
                                    
                                },
                                zoom: zoom
                            });
            document.getElementById("select").addEventListener("click", 
                this.selectPanoChangeMode.bind(this, 0));
            document.getElementById("rotate").addEventListener("click", 
                this.selectPanoChangeMode.bind(this, 1));
            document.getElementById("drag").addEventListener("click", 
                this.selectPanoChangeMode.bind(this, 2));
            document.getElementById("delete").addEventListener("click", 
                this.selectPanoChangeMode.bind(this, 3));
            this.selectPanoChangeMode(0);
        }
    }

    selectPanoChangeMode(mode) {
        this.mapMgr.panoChangeMode = mode;
        switch(mode) {
            case 0:
                document.getElementById('select').classList.add('selected');
                document.getElementById('rotate').classList.remove('selected');
                document.getElementById('drag').classList.remove('selected');
                document.getElementById('delete').classList.remove('selected');
                break;
            case 1:
                document.getElementById('rotate').classList.add('selected');
                document.getElementById('drag').classList.remove('selected');
                document.getElementById('select').classList.remove('selected');
                document.getElementById('delete').classList.remove('selected');
                break;
            case 2:
                document.getElementById('drag').classList.add('selected');
                document.getElementById('rotate').classList.remove('selected');
                document.getElementById('select').classList.remove('selected');
                document.getElementById('delete').classList.remove('selected');
                break;
            case 3:
                document.getElementById('delete').classList.add('selected');
                document.getElementById('rotate').classList.remove('selected');
                document.getElementById('select').classList.remove('selected');
                document.getElementById('drag').classList.remove('selected');
                break;
        }
    }
            

    // lifted from old otv
    setupUpload () {
        document.getElementById('uploadpano').addEventListener ("click",
            this.openDialog.bind(this));
    }

    openDialog() {
        const filterer = new Filterer(20.0);
        var uploader = new Uploader("panoUpload", "panorama",
                    { heading: "Upload panorama",
                    uploadLabel: "Agree and upload",
                    dialogDivId: 'dlgPanoUpload',
                    successMsg: 'Successfully uploaded. You will be able to view your panos but they will need to be approved by an administrator (1-2 days) for others to view them.',
                    content:
                        "<p>Panoramas should be 360 panoramas (either complete sphere or cylindrical) in equirectangular projection. Any non-panoramic photos or panoramas with sensitive content (e.g. faces, numberplates) may be deleted by site administrators. Panoramas should be on off-road routes; <strong>any panoramas on roads may be deleted by administrators to preserve space on the server.</strong> Panoramas will be copyright OpenTrailView 360 contributors and licenced under CC-BY 4.0. </p>"+
"<p>Please agree to this <strong>privacy policy:</strong>Your panoramas will be associated with your user account ID, or, if using your OpenStreetMap account, your OSM numerical user ID in the OpenTrailView database. The OpenTrailView API provides this information and thus it is possible to find out, via the API, which panoramas were contributed by which OSM users. The reason for associating panoramas with user ID is to allow you to view your own panoramas, and to allow you to modify your own panoramas, but not anyone else\'s.</p>",
                    style:
                        { 
                        top: '10px',
                        backgroundColor: "rgba(64,160,64,0.7)",
                        color: "white" },
                        url: "panorama/upload",
                        additionalFormInput: {
                    },
                    additionalContent: "<input type='checkbox' id='snap' checked='true' /><label for='snap'>Snap panoramas to the nearest path on OpenStreetMap, if within 20m.</label><br /><input type='checkbox' id='roadcheck' checked='true' /><label for='roadcheck'>Reject panoramas within 20m of a road, if the road is nearer than any path (RECOMMENDED).</label><br /><p><label for='filterDist'>Minimum distance between panoramas (panos closer than this will be filtered out):</label><br /><input type='range' id='filterDist' min='0' max='100' value='20' step='5' /><span id='filterDistValue'></span></p>",
                    onAddAdditionalContent: () => {
                        const range = document.getElementById('filterDist');
                        const roadcheck = document.getElementById('roadcheck');
                        const snap = document.getElementById('snap');
                        document.getElementById('filterDistValue').innerHTML = `${range.value}m`;
                        filterer.filterRoads = roadcheck.checked; 
                        filterer.snap = snap.checked;
                        range.addEventListener("change", e=> {
                            document.getElementById('filterDistValue').innerHTML = `${e.target.value}m`;
                            filterer.limit = parseFloat(e.target.value);
                        });
                        roadcheck.addEventListener("change", e=> { 
                            filterer.filterRoads = e.target.checked;
                        });
                        snap.addEventListener("change", e=> { 
                            filterer.snap = e.target.checked;
                        });
                    },
                    filterer: filterer.fullFilter.bind(filterer),
                    multiple: true
            });
                         
        filterer.setProgressHandler( msg => {
            uploader.setMessage(`<strong>FILTERING:</strong> ${msg}`); 
        });
        uploader.dialog.div.id = 'dlgPanoUpload';
        uploader.show();
    }
        
    setupAbout() {
        this.aboutDlg = new Dialog('viewContainer',
                { 'OK': ()=> { this.aboutDlg.hide(); }},
                {
                backgroundColor: "rgba(128,192,128,0.9)",
                color: "white",
                padding: '20px',
                //overflow: 'auto',
                 textAlign: "left" });
        this.aboutDlg.div.id = 'dlgAbout';
        fetch('views/about.html')
            .then(resp=>resp.text())
            .then(txt => { this.aboutDlg.setContent(txt);
        });
        this.setupAboutClick();
    }

    setupAboutClick() {
        document.getElementById('about').addEventListener('click', this.aboutDlg.show.bind(this.aboutDlg));
    }    

    setupSearch() {
        document.getElementById('search').addEventListener('click', e=> {
            e.stopPropagation();
        });
        document.getElementById('searchResults').addEventListener('click', e=> {
            e.stopPropagation();
        });

        document.getElementById('search').addEventListener('keyup', e=> {
            if(e.key == 'Enter') {
                var q = document.getElementById('q').value;
                this.nominatimSearch(q);
            }
        });

        document.getElementById('searchBtn').addEventListener('click', e=> {
            var q = document.getElementById('q').value;
            this.nominatimSearch(q);
        });
    }

    nominatimSearch(q) {
        fetch(`nomproxy.php?q=${q}`)
                .then(response=>response.json())
                .then(json=> {
                    var nodes = json.filter(o => o.lat != undefined && o.lon != undefined);
                    if(nodes.length==0) {
                        document.getElementById('searchResults').innerHTML = `No results for ${q}!`;
                    } else {
                        document.getElementById('searchResults').innerHTML = '';       
                        var p = document.createElement('p');
                        var strong = document.createElement("strong"); 
                        strong.appendChild(document.createTextNode("Search results from OSM Nominatim"));
                        p.appendChild(strong);
                        document.getElementById('searchResults').appendChild(p);
                        document.getElementById('searchResults').style.display = 'block';    
                        nodes.forEach(o=> {
                            var p = document.createElement('p');
                            p.style.margin = '0px';
                            var a = document.createElement('a');
                            a.href='#';
                            a.innerHTML = o.display_name;
                            a.addEventListener('click', e=> {
                                this.mapMgr.setView([o.lat, o.lon]);
                                document.getElementById('searchResults').style.display='none';    
                            });
                            p.appendChild(a);
                            document.getElementById('searchResults').appendChild(p);

                        });
                }
          });                  
    }

    managePhotos() {
        this.previousMode = this.mode;
        this.setupPhotoMgmt(true);

        this.mapMgr.setView([this.lat, this.lon]);

        if(!this.photoMgrDlg) {
            this.photoMgrDlg = new Dialog(document.body,
                {
                    'Close': ()=> { 
                    this.photoMgrDlg.hide();
                    this.setupMode(this.previousMode);
                    this.setupPhotoMgmt(false);
                }},
                { backgroundColor: "rgba(128,192,128,0.9)",
                color: "white",
                textAlign: "center" });
            this.photoMgrDlg.div.id = 'dlgPhotoMgr';
            var content = document.createElement("div");
            var h2 = document.createElement("h2");
            h2.appendChild(document.createTextNode("Manage your panoramas"));
            content.appendChild(h2);
            var p = document.createElement("p");
            p.appendChild(document.createTextNode("Select a panorama and then position it by clicking on the map. When you are finished, click 'Upload positioned panos' to upload them."));
            content.appendChild(p);
            var photoMgr = document.createElement("div");
            photoMgr.id="photoMgr";
            content.appendChild(photoMgr);

            this.photoMgrDlg.setDOMContent(content);
            this.photoMgrDlg.show();

            this.photoMgr = new PhotoManager(3,3,'photoMgr', { 
                actionsContainer: this.photoMgrDlg.actionsContainer, 
                onPositioned: (id,lat,lon)=> { 
                    this.mapMgr.addNewPano(id, lat, lon) 
                }, 
                onPositionUploaded: this.mapMgr.removeNewPanos.bind(this.mapMgr), 
                adminProvider: this,
                onSelected: id=>{
                    this.mapMgr.selectNewPano(id) 
            }});
        } else {
            this.photoMgrDlg.show();
        }
        
        this.mapMgr.map.on("click", e=> {
            this.photoMgr.setCoords(e.latlng);
        });
    }


    setupPhotoMgmt(on) {
        if(on) {
            document.getElementById('main').classList.add('photomgr');
            document.getElementById('map').classList.add('photomgr');
            document.getElementById('map').classList.remove('preview');
        } else {
            document.getElementById('main').classList.remove('photomgr');
            document.getElementById('map').classList.remove('photomgr');
        }
        document.getElementById('heading').style.display= on ? 'none' : 'block';
        document.getElementById('viewContainer').style.height= on ? '100%': 'calc(100% - 100px)';
        if(on) {
            this.mapMgr.map.invalidateSize();
        }
        if(on) {
            document.getElementById('panorama').style.display = 'none';
            document.getElementById('searchContainer').style.display = 'block';
        } else if (this.mode==0) {
            document.getElementById('panorama').style.display = 'block';
            document.getElementById('map').classList.add('preview');
        } else if (this.mode==1) {
            document.getElementById('searchContainer').style.display = 'block';
        }
    }

    setupMediaQueries() {
        this.mq = window.matchMedia("(max-width: 600px)");
        this.isMobile = this.mq.matches;
        this.mq.addListener ( mq=> {
            this.isMobile = mq.matches;
        });
    }

    setupMapPreview() {
        document.getElementById('map').classList.add('preview');
        this.mapMgr.map.invalidateSize();
    }
    
    setupLogin() {
        this.loginDlg = new Dialog('main',
                { 'Login': this.processLogin.bind(this),
                'Cancel': ()=> { this.loginDlg.hide(); }},
                {
                backgroundColor: "rgba(128,192,128,0.9)",
                color: "white",
                textAlign: "center" });
        this.loginDlg.setContent('<h2>Login</h2>'+
            "<p><span id='loginError' class='error'></span><br />"+
            "<label for='username'>Email address</label><br />" +
            "<input id='username' type='text' /> <br />"+
            "<label for='password'>Password</label><br />" +
            "<input id='password' type='password' /> </p>"+
            "<p><a href='osmLogin' id='loginOsm'>Login with OSM</a></p>");
        this.loginDlg.div.id='dlgLogin';
        
        fetch ('login').then(resp => resp.json()).then(json => {
                this.username = json.username;
                this.userid = json.userid;
                this.isadmin = json.isadmin;
                this.onLoginStateChange();
        });
        this.username = null;
        this.userid = 0; 
        this.isadmin = 0; 
        this.onLoginStateChange();
    }    

    setupSignup() {
        this.signupDlg = new Dialog('main',
                { 'Signup': this.processSignup.bind(this),
                'Close': ()=> { this.signupDlg.hide(); }},
                {
    
                backgroundColor: "rgba(128,192,128,0.9)",
                color: "white", padding: '10px',
                textAlign: "center" });
        this.signupDlg.setContent(
"<h2>Sign up</h2>"+
"<p id='signupMsg' class='error'></p>"+
"<div id='signup_content'>"+
"<p>Signing up will allow you to upload panoramas, view and position your "+
"existing panoramas, and adjust your panoramas (such as rotate and "+
"move them).</p>" +
"<p><strong>Please do NOT use a password that you use "+
"elsewhere. While this site has standard security measures implemented it is "+
"a personal rather than a corporate site, and "+
"therefore has not had a full security audit carried out by a security professional.</strong></p>"+
"<label for='username'>"+
"Enter your email address:"+
"</label>"+
"<br />"+
"<input name='signup_username' id='signup_username' type='text' />"+
"<br /> <label for='singup_password'>Enter a password: </label> <br />"+
"<input name='signup_password' id='signup_password' type='password'/>" +
"<br /> <label for='password2'>Re-enter your password: </label> <br />"+
"<input name='password2' id='password2' type='password'/>"+
"<p>Please note that the panoramas will be copyright 'OpenTrailView 360 "+
"contributors' and licensed under Creative Commons-Attribution (CC-BY) 4.0."+
"However, as the developer of OpenTrailView, <em>I am explicitly allowing "+
"all panoramas to be used as source material for OpenStreetMap.</em></p>"+
"</div>");

        this.signupDlg.div.id = 'dlgSignup';
    }    


    setupReportPrivacy() {
        document.getElementById('reportPrivacyConcern')
            .addEventListener('click', e=> { 
            if(!this.reportPrivacyDlg) {
                this.reportPrivacyDlg = new Dialog('main',
                    { 'OK': () => {
                        const json=JSON.stringify({"msg": document.getElementById('privacyMsg').value});
                        fetch(`panorama/${this.panoView.curPanoId}/reportPrivacyConcern`, { method: 'POST', headers: {'Content-Type': 'application/json'}, body: json })
                        .then( response => {
                            alert('Your privacy concern has been sent. We will deal with it as soon as possible.');
                            this.reportPrivacyDlg.hide();
                        });
                    },
                    'Cancel': ()=> { this.reportPrivacyDlg.hide(); }
                    },
                    {
                        top: '10px',
                        left: 'calc(50% - 200px)',
                        width: '400px',
                        height: '400px',
                        backgroundColor: "rgba(128,192,128,0.9)",
                        color: "white",
                        textAlign: "center" 
                    });
                this.reportPrivacyDlg.setContent('<h2>Report a privacy issue</h2><p>All panoramas are run through face and license plate detection software or checked by administrators. However, it is possible that the software did not detect sensitive content in a small minority of cases. Please, therefore, let us know your privacy concerns about the current panorama. We will remove the panorama if necessary.</p><p><textarea id="privacyMsg" style="width: 90%; left: 10%; height: 100px"></textarea></p>');
            }
        
            this.reportPrivacyDlg.show();
        });
    }
    
    processLogin() {
        var json=JSON.stringify({"username": document.getElementById("username").value,  "password": document.getElementById("password").value});
        fetch('login', { method: 'POST', headers: {'Content-Type': 'application/json'}, body:json})
            .then(res => {
                if(res.status == 401) {
                   throw('Incorrect login.');
                }
                else return res.json();
             }) 
             .then(json => {
                if(json.error) {
                    document.getElementById("loginError").innerHTML = json.error;
                } else if(json.userid) {
                    this.username = json.username;
                    this.userid = json.userid;
                    this.isadmin = json.isadmin;
                    this.loginDlg.hide();
                    this.onLoginStateChange();
                    this.checkAuthorised(this.panoView.curPanoId);
                }
             })
            .catch(e => { 
                document.getElementById('loginError').innerHTML = e;
            });
    }

    processSignup() {
        var json=JSON.stringify({"username": document.getElementById("signup_username").value,  "password": document.getElementById("signup_password").value, "password2": document.getElementById("password2").value});
        fetch('signup', { method: 'POST', headers: {'Content-Type': 'application/json'}, body:json})
                .then(res => res.json())
                .then(json => {
                    if(json.error) {
                        document.getElementById("signupMsg").innerHTML = json.error;
                    } else if(json.username) {
                        document.getElementById("signupMsg").innerHTML = `Successfully signed up as ${json.username}`; 
                    } else {
                        document.getElementById("signupMsg").innerHTML = 'Failed to add your details.';
                    }
        });
    }

    onLoginStateChange(){
        if(this.userid) {
            this.onLogin();
        } else {
            this.onLogout();
        }
        if(this.photoMgr) {
            this.photoMgr.reloadModeSelect();
        }
        if(this.mapMgr && this.mapMgr.initialised) {
            this.mapMgr.clearMarkers();
            this.mapMgr.loadPanoramas();
        }
    }

    onLogin() {
        document.getElementById("loginContainer").innerHTML = "";
        var t = document.createTextNode(`Logged in as ${this.username}`);
        var a = document.createElement("a");

        a.id="managePhotos";
        a.addEventListener("click", this.managePhotos.bind(this));
        a.appendChild(document.createTextNode(" "));
        a.appendChild(document.createTextNode("Manage photos"));

        document.getElementById("loginContainer").appendChild(t);
        document.getElementById("loginContainer").appendChild(a);

        if(this.isadmin) {
            a = document.createElement('a');
            a.id="authorisePano";
            a.addEventListener("click", async(e)=> {
                const id = this.panoView.curPanoId;
                const json = await fetch(`panorama/${id}`)
                    .then(response => response.json());
                fetch(`panorama/${id}/authorise`, { method: 'POST' })
                    .then( response => { 
                        if(response.status==200) {
                            alert(`Pano ${id} authorised successfully.`);
                            e.target.style.display = 'none';
                        } else {
                            alert(`Server error: code ${response.status}`);
                        }
                });
            }); 
            a.appendChild(document.createTextNode(" "));
            a.appendChild(document.createTextNode("Authorise pano"));

            document.getElementById("loginContainer").appendChild(a);
        }

        a = document.createElement("a");
        a.id="logout";
        a.addEventListener("click", this.logout.bind(this));
        a.appendChild(document.createTextNode(" "));
        a.appendChild(document.createTextNode("Logout"));
        document.getElementById("loginContainer").appendChild(a);
        if(this.mode == 0) {
            document.getElementById("uploadpano").style.display = "inline";
        }
        if(this.mode == 1) {
            document.getElementById("drag").style.display = "inline";
            document.getElementById("rotate").style.display = "inline";
            document.getElementById("delete").style.display = "inline";
        }
        this.mapMgr.activated = true;    
    }

    onLogout() {
        document.getElementById("loginContainer").innerHTML = "";
        var as = document.createElement("a");
        as.id="signup";
        as.addEventListener("click", this.signupDlg.show.bind(this.signupDlg));
        as.appendChild(document.createTextNode("Sign up"));
        var al = document.createElement("a");
        al.id="login";
        al.addEventListener("click", ()=> {
            this.loginDlg.show();
        });
        al.appendChild(document.createTextNode("Login"));
        document.getElementById("loginContainer").appendChild(as);
        document.getElementById("loginContainer").appendChild(document.createTextNode(" | "));
        document.getElementById("loginContainer").appendChild(al);
        document.getElementById("uploadpano").style.display = "none";
        document.getElementById("drag").style.display = "none";
        document.getElementById("rotate").style.display = "none";
        document.getElementById("delete").style.display = "none";
        this.mapMgr.activated = false;    
    } 

    logout() {
        fetch('logout', {method:"POST"}).then(resp=> {
            this.username = null;
            this.userid = this.isadmin = 0; 
            this.onLoginStateChange();
        });
    }
}

module.exports = OTV360;
