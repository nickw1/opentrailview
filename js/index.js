const OTV360 = require('./OTV360');
const qs = require('querystring');

const parts = window.location.href.split('?');
const get = qs.parse(parts[1]); 

const params = [ 'lat', 'lon', 'zoom' ];

params.forEach ( key => {
    if(get[key] === undefined && localStorage.getItem(key)!==null) {
        get[key] = localStorage.getItem(key);
    }
});


const otv = new OTV360(get.zoom ? get.zoom.replace('#', '') : 16, get.resize);
if(get.id) {
    otv.panoView.loadPanorama(get.id);
} else if (get.lat && get.lon) {
    otv.panoView.findPanoramaByLonLat(get.lon, get.lat);
} else {
    otv.panoView.loadPanorama(4);
}
