const jsfreemaplib = require('jsfreemaplib');
const exifr = require('exifr');

class Filterer {

    constructor(limit) {
        this.limit = limit || 20; 
        this.roads = ['motorway', 'trunk', 'primary', 'secondary', 'tertiary', 'unclassified', 'residential', 'motorway_link', 'trunk_link', 'secondary_link', 'tertiary_link', 'unclassified_link', 'residential_link' ];
        this.filterRoads = false;
        this.snap = false;
    }

    setProgressHandler(handler) {
        this.progressHandler = handler;
    }

    async fullFilter(files) {
        files = Array.from(files).filter (file => file.size < 83886080);
        const exifs = await this.filterPanos(files); 
        let files2 = { includedFiles: [] };
        if(this.filterRoads === true || this.snap === true) {    
            this.progressHandler('Filtering out panoramas on roads...');
            files2.warnings = exifs.warnings;
            for(let i=0; i<exifs.includedFiles.length; i++) {
                let json = { };
                if(exifs.includedFiles[i].latitude !== undefined && exifs.includedFiles[i].longitude !== undefined) {
                    json = await fetch (`nearestHighway?lat=${exifs.includedFiles[i].latitude}&lon=${exifs.includedFiles[i].longitude}&dist=20`).then(response => response.json());
                }
                if(this.filterRoads || json.highway === undefined || this.roads.indexOf(json.highway) === -1) {
                    files2.includedFiles.push({ 
                        file: exifs.includedFiles[i].file, 
                        lat: this.snap ? json.lat : undefined,
                        lon: this.snap ? json.lon : undefined
                    });
                } 
            }
            this.progressHandler(`${files2.length} panoramas not on roads.`);
        } else {
            files2 = exifs;//.map ( exif => exif.file );
        } 
        return files2;
    }

    async filterPanos(files) {
        this.startDate = new Date();
        let lastIncludedLat = 91;
        let lastIncludedLon = 181;
        let warnings = [];
        const includedFiles = [];
        const exifs = [];
        for(let i=0; i<files.length; i++) {
            const exif = await this.getExif(files[i], i);
            if(this.progressHandler && !(i % 10)) {
                this.progressHandler(`Processing EXIF data for file ${i+1} of ${files.length}`);
            }
            exif.file = files[i];
            exifs.push(exif);
        }
        exifs.sort ( (exif1, exif2) => exif1.timestamp - exif2.timestamp )
                .forEach ((exif, i) => {
            let included = true;
            if(this.progressHandler && !(i % 10)) {
                this.progressHandler(`Testing filter criteria for file ${i+1} of ${files.length}`);
            }
            if(i >= 1) {
                const dist = jsfreemaplib.haversineDist(exif.longitude, exif.latitude, lastIncludedLon, lastIncludedLat);
                included = dist >= this.limit;
            } 
            if(included === true) {
                lastIncludedLon = exif.longitude;
                lastIncludedLat = exif.latitude;
                includedFiles.push(exif);
            }
        });
        return {
            includedFiles : includedFiles,
            warnings: warnings
        };
    }

    async getExif(file, i) {
        try {
            const exif = await file.arrayBuffer().then(exifr.parse);
            exif.timestamp = exif.DateTimeOriginal ? new Date(exif.DateTimeOriginal).getTime(): this.startDate + i; 
            return exif;
        } catch(e) {
            return { longitude: 0, 
                     latitude: 0, 
                     timestamp: this.startDate + i 
            };
        }
    }
}

module.exports = Filterer;
