<?php

//session_cache_limiter(false);
session_start();


//die("OpenTrailView is temporarily unavailable, please check back soon.");

require 'vendor/autoload.php';
require_once('defines.php');
require_once('controllers/PanoController.php');
require_once('controllers/UserController.php');
require_once('controllers/MapController.php');
require_once('controllers/OsmAuthController.php');
require_once('controllers/ClientAppController.php');
require_once("private/defines.php");
$setupCoreRoutes = require_once('routes/core_routes.php');
$setupOtvRoutes = require_once('routes/otv_routes.php');

$app = new \Slim\App(["settings"=>["displayErrorDetails"=>true]]);

$container = $app->getContainer();
$container['db'] = function() {
    $conn = new PDO("pgsql:host=localhost;dbname=".DB_DBASE, DB_USER);
    return $conn;
};
$container['view'] = new \Slim\Views\PhpRenderer('views');

$container["UserController"] = function($c) {
    return new UserController($c->get('db'));
};

$container["ClientAppController"] = function($c) {
    return new ClientAppController($c->get('view'), $c->get('router'), $c->get('db'));
};

$container["PanoController"] = function($c) {
    $db = $c->get('db');
    return new PanoController($c->get('db'));
};

$container["MapController"] = function($c) {
    $db = $c->get('db');
    return new MapController($c->get('db'));
};

$container["OsmAuthController"] = function($c) {
    return new OsmAuthController($c->get('view'), $c->get('router'));
};

$app->get('/', \OsmAuthController::class.":showPage")->setName('root'); 

$setupCoreRoutes($app);
$setupOtvRoutes($app);

$app->run();

?>
