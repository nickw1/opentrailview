<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class ClientAppController {

    protected $view, $router, $db;

    public function __construct($view, $router, $db) {
        $this->view = $view;
        $this->router = $router;
        $this->db = $db;
    }

    public function processAddApp(Request $req, Response $res, array $args) {
        $post = $req->getParsedBody();
        if(!isset($_SESSION["userid"])) {
            return $res->withRedirect($this->router->pathFor('root'));
        } elseif(preg_match("/^[\w\s]+$/", $post["app_name"]) && isset($post['redirect_uri']) && filter_var($post['redirect_uri'], FILTER_VALIDATE_URL)!==false) {
            $stmt = $this->db->prepare("INSERT INTO oauth_clients(client_id, client_secret, name, redirect_uri,grant_types,userid) VALUES (?,?,?,?,'authorization_code',?)");
            $clientId = bin2hex(random_bytes(16));
            $secret = bin2hex(random_bytes(32));
            $stmt->execute([$clientId, password_hash($secret, PASSWORD_DEFAULT), $post["app_name"], $post["redirect_uri"], $_SESSION['userid']]);
            return $this->view->render($res, 'add_app.phtml',["msg"=>"You added the app '$_POST[app_name]'.<br />Its client ID is $clientId.<br />Its secret is $secret.", "router"=>$this->router]);
        } else {
            return $this->view->render($res, 'add_app.phtml',["msg"=>"Either the app name or the redirect URI was in an invalid format. The app name should only contain letters, numbers and underscores.", "router"=>$this->router]);
        }
    }

    public function addApp(Request $req, Response $res, array $args) {
        if(!isset($_SESSION["userid"])) {
            return $res->withRedirect($this->router->pathFor('root'));
        } 
        return $this->view->render($res, 'add_app.phtml', ["router"=>$this->router]);
    }
}
?>
