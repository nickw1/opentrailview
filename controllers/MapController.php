<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require_once(dirname(__DIR__).'/models/MapModel.php');

class MapController {
    protected $model;

    public function __construct($db) {
        $this->model = new MapModel($db);
    }

    public function getByBbox(Request $req, Response $res, array $args) {
        $get = $req->getQueryParams();
        if(isset($get["bbox"])) {
            $bbox = array_filter( explode(",", $get["bbox"]), 
                function($val) { return preg_match("/^-?[\d\.]+$/", $val); }
            );
            return count($bbox)==4 ? $res->withJson($this->model->getByBboxLL($bbox)):
                $res->withStatus(400)->withJson(["error"=>"invalid bbox"]);
        } else {
            return $res->withStatus(400)->withJson(["error"=>"no bbox"]);
        }
    }


    // copied from PanoController
    public function getNearestHighway(Request $req, Response $res, array $args) {
        $get = $req->getQueryParams();
        if(preg_match("/-?[\d\.]+$/", $get["lon"]) && preg_match("/-?[\d\.]+$/", $get["lat"]) && preg_match("/-?[\d\.]+$/", $get["dist"])) {
            $highway = $this->model->findNearestHighway($get["lon"], $get["lat"], $get["dist"]);
            return $res->withJson($highway);
        } else {
            return $res->withStatus(400)->withJson(["error"=>"invalid params"]);
        }
    }
}
?>
