<?php

require_once('classes/OSMOAuthClient.php');
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class OsmAuthController {

    protected $view, $router;

    public function __construct($view, $router) {
        $this->view = $view;
        $this->router = $router;
    }

    public function login(Request $req, Response $res, array $args) {
        try {
            $oauth = new OSMOAuthClient();
            $requestToken = $oauth->login();
            return $res->withRedirect("https://www.openstreetmap.org/oauth/authorize?oauth_token=".$requestToken["oauth_token"]);
        } catch(OAuthException $e) {
            $error = $e->lastResponse;
            return $this->view->render($res, "index.phtml", ["router"=>$this->router, "error"=>$error]);
        }
    }

    public function showPage(Request $req, Response $res, array $args) {
        $user = $error = false;
        try {
            $inputData = $req->getQueryParams();
            $oauth = new OSMOAuthClient();
             if (self::loggedIn()) {
                $oauth->setSessionToken();
                $user = $oauth->getUser();
            } elseif (self::isOauthCallback($inputData)) {
                $at = $oauth->getAccessToken($inputData["oauth_token"], $_SESSION["request_secret"]);
                unset($_SESSION["request_secret"]);
                $_SESSION["access_token"] = $at["oauth_token"];
                $_SESSION["access_secret"] = $at["oauth_token_secret"];
                $oauth->setToken($_SESSION["access_token"], $_SESSION["access_secret"]);
                $user = $oauth->getUser();
                $_SESSION["userid"] = "o$user[id]"; 
                $_SESSION["username"] = $user["display_name"]; 
                return $res->withRedirect($this->router->pathFor('root')); // remove oauth token from url
            } 
        } catch(OAuthException $e) {
            $error = $e->lastResponse;
        }
        return $this->view->render($res, "index.phtml", ["router"=>$this->router, "error"=>$error, "user"=>$user["display_name"]]);
        
    }

    public function logout(Request $req, Response $res, array $args) {
        session_destroy();
        return $res->withRedirect($this->router->pathFor('root'));
    }

    static function loggedin() {
        return isset($_SESSION["access_token"]) && isset($_SESSION["access_secret"]);
    }

    static function isOauthCallback($inputData)  {
        return isset($inputData["oauth_token"]) && isset($_SESSION["request_secret"]);
    }
    
    public function loginStatus(Request $req, Response $res, array $args) {
        return $res->withJSON(["userid"=>isset($_SESSION["userid"]) ? $_SESSION["userid"]: 0]);
    }
}
?>
