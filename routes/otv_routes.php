<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

return function($app) {
    $app->get('/panorama/{id}/nearby', \PanoController::class.":getNearby");
    $app->get('/nearestHighway', \MapController::class.":getNearestHighway");
    $app->get('/map', \MapController::class.":getByBbox");


    $app->get('/osmLogin', \OsmAuthController::class.':login'); 
    $app->get('/addApp', \ClientAppController::class.":addApp");
    $app->post('/addApp', \ClientAppController::class.":processAddApp")->setName('processAddApp');

    $app->get('/apidoc', function(Request $req, Response $res, array $args) {
        return $this->view->render($res,"apidoc.html");
    });
}

?>

