<?php

return function($app) {
    $app->get('/panorama/{id:[0-9]+}', \PanoController::class.":getById");
    $app->delete('/panorama/{id:[0-9]+}', \PanoController::class.":deletePano");
    $app->post('/panorama/{id}/authorise', \PanoController::class.":authorisePano");
    $app->get('/nearest/{lon}/{lat}', \PanoController::class.":getNearest"); 
    $app->get('/panos', \PanoController::class.":getByBbox"); 
    $app->get('/panos/unpositioned', \PanoController::class.":getUnpositioned"); 
    $app->get('/panos/mine', \PanoController::class.":getAllByUser"); 
    $app->get('/panos/unauthorised', \PanoController::class.":getUnauthorised"); 
    $app->post('/panorama/{id}/rotate', \PanoController::class.":rotate"); 
    $app->post('/panorama/{id}/move', \PanoController::class.":move");
    $app->post('/panoramas/move', \PanoController::class.":moveMulti");
    $app->post('/panorama/upload', \PanoController::class.":uploadPano");
    $app->post('/panorama/{id}/reportPrivacyConcern', \PanoController::class.":reportPrivacyConcern");
    $app->get('/login', \UserController::class.':getLogin'); 
    $app->post('/login', \UserController::class.':login');
    $app->post('/logout', \UserController::class.':logout');
    $app->post('/signup', \UserController::class.':signup');
}

?>
