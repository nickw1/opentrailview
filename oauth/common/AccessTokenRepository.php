<?php

use League\OAuth2\Server\Entities\AccessTokenEntityInterface;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Repositories\AccessTokenRepositoryInterface;

class AccessTokenRepository implements AccessTokenRepositoryInterface {
	protected $db;

	public function __construct($db) {
		$this->db = $db;
	}

	public function getNewToken(ClientEntityInterface $clientEntity, array $scopes, $userIdentifier=null) {
		// return something which implements AccessTokenEntityInterface
		$accessToken = new AccessTokenEntity();
		$accessToken->setClient($clientEntity);
		foreach($scopes as $scope) {
			$accessToken->addScope($scope);
		}
		$accessToken->setUserIdentifier($userIdentifier);
		return $accessToken;
	}

	public function persistNewAccessToken(AccessTokenEntityInterface $accessToken) {
		//$accessTolen->getScopes(): ScopeEntityInterface[]
		$scopes = $accessToken->getScopes();
		$allScopes = [];
		foreach($scopes as $scope) {
			$allScopes[] = $scope->getIdentifier();
		}
		$scopes = implode(" ", $allScopes);
		$stmt = $this->db->prepare("INSERT INTO oauth_access_tokens(access_token, client_id, userid, expires, scope) VALUES (?,?,?,?,?)");
		$stmt->execute([$accessToken->getIdentifier(), $accessToken->getClient()->getIdentifier(), $accessToken->getUserIdentifier(), $accessToken->getExpiryDateTime()->getTimestamp(), $scopes]);
	}

	public function revokeAccessToken($tokenId) {
		$stmt = $this->db->prepare("UPDATE oauth_access_tokens SET revoked=true WHERE access_token=?");
		$stmt->execute([$tokenId]);
	}

	public function isAccessTokenRevoked($tokenId) {
		$stmt = $this->db->prepare("SELECT revoked FROM oauth_access_tokens WHERE access_token=?");
		$stmt->execute([$tokenId]);
		$row = $stmt->fetch();
		return $row ? $row["revoked"] : true;
	}
}

?>
