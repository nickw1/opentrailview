<?php

use League\OAuth2\Server\Entities\ScopeEntityInterface;
use League\OAuth2\Server\Entities\Traits\EntityTrait;
use League\OAuth2\Server\Entities\Traits\ScopeTrait;

class ScopeEntity implements ScopeEntityInterface {
    use EntityTrait, ScopeTrait;

    protected $description;

    function __construct($id, $description) {
        $this->identifier = $id;
        $this->description = $description;
    }
}


?>
