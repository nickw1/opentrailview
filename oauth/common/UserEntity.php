<?php

use League\OAuth2\Server\Entities\UserEntityInterface;
use League\OAuth2\Server\Entities\Traits\EntityTrait;

class UserEntity implements UserEntityInterface {

	use EntityTrait;

	function __construct($db, $id) {
		$this->db =$db;
		$this->identifier = $id;
	}
}

?>
