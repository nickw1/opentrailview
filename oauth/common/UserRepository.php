<?php

use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Repositories\UserRepositoryInterface;

class UserRepository implements UserRepositoryInterface {
	protected $db;

	public function getUserEntityByUserCredentials($user, $pass, $grantType, ClientEntityInterface $clientEntity) {
		$stmt = $this->db->prepare("SELECT id,password FROM users WHERE username=? AND allowed_grant_types LIKE ?");
		$grantPattern = "%$grantType%";
		$stmt->execute([$user, $grantPattern]);
		$row = $stmt->fetch();
		return $row!==false && password_verify($pass, $row["password"])!==false ? new UserEntity($row["id"]) : null;
	}

}

?>
