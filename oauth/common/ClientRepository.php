<?php

use League\OAuth2\Server\Repositories\ClientRepositoryInterface;

class ClientRepository implements ClientRepositoryInterface {
	protected $db;

	public function __construct($db) {
		$this->db = $db;
	}

	function getClientEntity($clientIdentifier) {
		$client = new ClientEntity();
		$stmt = $this->db->prepare("SELECT name,redirect_uri FROM oauth_clients WHERE client_id=?");
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		$client->setIdentifier($clientIdentifier);
		$client->setName($row["name"]);
		$client->setRedirectUri($row["redirect_uri"]);
		$client->setConfidential();
		return $client;
	}

	function validateClient($clientIdentifier, $clientSecret, $grantType) {
		$stmt = $this->db->prepare("SELECT client_secret FROM oauth_clients WHERE client_id=? AND grant_types LIKE ?");
		$grantPattern = "%$grantType%";
		$stmt->execute([$clientIdentifier, $grantPattern]);
		$row = $stmt->fetch();
		return $row!==false && password_verify($clientSecret, $row["client_secret"])!==false;
	}
}

?>
