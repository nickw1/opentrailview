<?php

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;


require('vendor/autoload.php');
require_once('../../defines.php');
require_once('../common/AccessTokenRepository.php');
require_once('../../controllers/PanoController.php');

session_start();

$app = new \Slim\App(["settings"=>["displayErrorDetails"=>true]]);

$container = $app->getContainer();

$container['db'] = function() {
    $conn = new PDO("pgsql:host=localhost;dbname=gis", "gis");
    $container["conn"] = $conn;
    return $conn;
};

$container["PanoController"] = function($c) {
    $db = $c->get('db');
    return new PanoController($c->get('db'));
};

$container['server'] = function($c) use($app) {
    $db = $c->get('db');
    $accessTokenRepository = new AccessTokenRepository($db);
    $publicKeyPath="file:///var/www/html/opentrailview.org/oauth/auth/public.key";
    $server = new \League\OAuth2\Server\ResourceServer(
                        $accessTokenRepository,
                        $publicKeyPath
                    );
    return $server;
};

$app->add(new \League\OAuth2\Server\Middleware\ResourceServerMiddleware($container->get('server')));


$app->post('/panorama/upload', \PanoController::class.":uploadPanoOAuth");
$app->delete('/panorama/{id}', \PanoController::class.":deletePanoOAuth");
$app->post('/note/upload', \PanoController::class.":uploadNoteOAuth");
$app->delete('/note/{id}', \PanoController::class.":deleteNoteOAuth");
$app->post('/panorama/{id}/rotate', \PanoController::class.":rotateOAuth");
$app->post('/panorama/{id}/move', \PanoController::class.":moveOAuth");

$app->post('/tag/create', function(Request $req, Response $res, array $args) {
    $post = $req->getParsedBody();
    $stmt = $this->db->prepare("INSERT INTO tags(panoid,tag) VALUES (?,?)");
    $stmt->execute([$post['panoid'], $post['tag']]);
    return $res->getBody()->write('OK');
});

$app->run();

?>
